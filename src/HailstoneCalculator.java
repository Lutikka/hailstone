import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.regex.Pattern;

/**
 * <pre>
 * This class is used to calculate some results for a hailstone sequence.
 * 
 * Results include:
 * -Number of steps until convergence to number one
 * -Second largest number in the sequence
 * 
 * Behavior is undefined for extremely large n.
 * </pre>
 * @author Erkki Sinisalo
 *
 */
public class HailstoneCalculator {
	

	//Helper variables to avoid constructing them over and over
	public static final BigInteger ONE = new BigInteger("1");
	public static final BigInteger TWO = new BigInteger("2");
	public static final BigInteger THREE = new BigInteger("3");
	
	/**
	 * 
	 * @param args initial number for the hailstone sequence
	 */
	public static void main(String[] args){
		//input checking
		if(args.length==0){
			System.out.println("Missing required argument");
			return;
		}
		Pattern pattern = Pattern.compile("[\\d]*");
		if(!pattern.matcher(args[0]).matches()){
			System.out.println("Invalid Argument use chars {0-9}");
			return;
		}
		
		HailstoneCalculator hailStone = new HailstoneCalculator();
		Result r = hailStone.hailstoneSequence(new BigInteger(args[0]));
		
		//Set the html template to a backup version
		String html = "<div><h1> Results </h1><p>Input: ###input </p><p>Iterations:"
				+ " ###iterations </p><p>Second Largest Number: ###secondLargestNumber </p></div>";
		
		//Read template for the page
		try {
			html = readFile("template.html",StandardCharsets.UTF_8);
		} catch (Exception e) {
			System.out.println("Error while reading template.html - using backup template");
		}
		
		//Replace fields in the template
		html= html.replaceAll("###input", r.input);
		html= html.replaceAll("###iterations", r.iterations);
		html= html.replaceAll("###secondLargestNumber", r.secondLargestNumber.toString());
		html= html.replaceAll("###sequence", r.sequence.toString());
		
		//output the html file
		File f = new File("results.html");
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(f));
			bw.write(html);
			bw.close();
			System.out.println("Output in results.html");
		} catch (Exception e) {
			System.out.println("Error while writing to results.html - outputting in terminal instead");
			System.out.println(r);
		} 
		System.out.println("Success!");
		
	}
	
	private static String readFile(String path, Charset encoding) throws IOException, URISyntaxException
	{
		InputStream istream = HailstoneCalculator.class.getResourceAsStream(path);
		BufferedReader reader = new BufferedReader(new InputStreamReader(istream));
        StringBuilder out = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            out.append(line);
        }
        return out.toString();
	}
	
	/**
	 * Calculates the hailstone sequence for given natural number n.
	 * @param n a natural number
	 * @return the results as a {@link Result} class
	 */
	public Result hailstoneSequence(BigInteger n){
		Result r = hailstoneSequence(n,0, new TwoLargestTracker(), new Result()); 
		r.setInput(""+n);
		return r;
	}
	
	private Result hailstoneSequence(BigInteger n, int iterations, TwoLargestTracker tracker, Result r){
		//update sequence
		r.appendToSequence(n);
		//update largest numbers tracker
		tracker.offer(n);
		
		//Check if the sequence converged in this iteration
		if(n.equals(ONE)){
			return r.setIterations(""+iterations).setSecondLargestNumber(tracker.getSecondLargest());
		}
		
		//Determine next step
		if(n.mod(TWO).equals(ONE)){
			//odd
			return hailstoneSequence(n.multiply(THREE).add(ONE),iterations+1,tracker,r);
		}else{
			//even
			return hailstoneSequence(n.divide(TWO),iterations+1,tracker,r);
		}

		
	}
	
	/**
	 * <pre>
	 * TwoLargestTracker is a class for tracking the two largest elements.
	 * 
	 * Usage: First sequentially offer a number of BigInteger elements using the {@link #offer(BigInteger)} method. 
	 * Then use methods {@link #getLargest()} and {@link #getSecondLargest()} to get the largest two elements offered so far.
	 * 
	 * The largest and the secondLargest elements are initialized to be equal to 1.
	 * </pre>
	 * @author Erkki Sinisalo
	 *
	 */
	public class TwoLargestTracker{
		private BigInteger largest = new BigInteger("1");
		private BigInteger secondLargest = new BigInteger("1");
		
		/**
		 * 
		 * @return the largest number so far
		 */
		public BigInteger getLargest(){
			return largest;
		}
		
		/**
		 * 
		 * @return the second largest number so far
		 */
		public BigInteger getSecondLargest(){
			return secondLargest;
		}
		
		/**
		 * <pre>
		 * Offer this item to the tracker causing the largest
		 * and/or second largest element to change if the given
		 * item larger than one of them.
		 * </pre>
		 * @param item to be offered
		 */
		public void offer(BigInteger item){
			if(largest.compareTo(item)==-1){
				//largest < item
				secondLargest = largest;
				largest=item;
			}else if(secondLargest.compareTo(item)==-1){
				//second largest < item
				secondLargest=item;
			}
		}
	}
	
	/**
	 * Wrapper class for the results of hailstone sequence computation
	 * @author Erkki Sinisalo
	 *
	 */
	public class Result{
		
		/**
		 * The number n given as input
		 */
		public String input;
		
		/**
		 * The number of iterations before convergence to 1
		 */
		public String iterations; 
		
		/**
		 * The second largest number in the sequence
		 */
		public BigInteger secondLargestNumber;
		
		/**
		 * The sequence of numbers
		 */
		public StringBuilder sequence=null;
		
		/**
		 * Appends the element to the sequence
		 * @param b the number to be appended
		 * @return the Result instance
		 */
		public Result appendToSequence(BigInteger b){
			if(sequence==null){
				sequence = new StringBuilder();
				sequence.append(b);
			}else{
				sequence.append(", "+b);
			}
			
			return this;
		}
		/**
		 * 
		 * @param input the input to be set
		 * @return the Result instance
		 */
		public Result setInput(String input){
			this.input=input;
			return this;
		}
		/**
		 * 
		 * @param iterations the iterations to be set
		 * @return the Result instance
		 */
		public Result setIterations(String iterations){
			this.iterations=iterations;
			return this;
		}
		/**
		 * 
		 * @param secondLargestNumber the second largest number to be set
		 * @return the Result instance
		 */
		public Result setSecondLargestNumber(BigInteger secondLargestNumber){
			this.secondLargestNumber=secondLargestNumber;
			return this;
		}

		@Override
		public String toString(){
			StringBuilder stringRep = new StringBuilder((50));
			stringRep.append("\n");
			stringRep.append("Result: \n");
			stringRep.append("Input: "+input+"\n");
			stringRep.append("Iterations: "+iterations+"\n");
			stringRep.append("Second Largest Number: "+secondLargestNumber+"\n");
			return stringRep.toString();
		}
	}
}


