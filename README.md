# Hailstone

Calculates the hailstone sequence for a given number returning the following metrics: 

-Input number
-number of iterations
-second largest number.


Example execution:

java -cp hailstone.jar HailstoneCalculator 57385734895734853748537450835083457083455846784567845967845678457689457689456788595656

or

java -cp hailstone.jar HailstoneCalculator 27


27 is an example input